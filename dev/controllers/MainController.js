import {controllers} from 'tramway-core-react';
import App from '../components/App';
import os from 'os';

let {ReactController} = controllers;

/**
 * @class MainController
 * @extends {Controller}
 */
export default class MainController extends ReactController {
    /**
     * @static
     * @param {Object} req
     * @param {Object} res
     * @memberOf Main
     */
    static index (req, res) {
        return ReactController.render(res, App, {'message': `Page rendered from ${os.hostname()}`});
    }

    /**
     * @static
     * @param {Object} req
     * @param {Object} res
     * @memberOf Main
     */
    static sayHello(req, res) {
        return ReactController.render(res, App, {'message': `Hello ${req.params.name}`});
    }
}