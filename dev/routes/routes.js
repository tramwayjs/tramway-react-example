import MainController from "../controllers/MainController";
import StandardAuthenticationPolicy from "../policies/StandardAuthenticationPolicy";

const routesValues = [
    {
        "methods": ["get"],
        "controller": MainController.index
    },
    {
        "path": "/hello",
        "arguments": ["name"],
        "methods": ["get"],
        "controller": MainController.sayHello
    },
    {
        "arguments": ["name"],
        "methods": ["get"],
        "controller": MainController.sayHello
    }
];

export default routesValues;