'use strict';

import express from 'express';
import bodyParser from 'body-parser';
import cors from './config/cors.js';
import methodOverride from 'method-override';
import cookieParser from 'cookie-parser';
import {Router} from 'tramway-core';
import routes from './routes/routes.js';

const PORT = 8080;

let app = express();

app.use(express.static(__dirname + '/public'));
app.set('views', 'dist/views');
app.set('view engine', 'ejs');

app.use(methodOverride('_method'));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cors);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

let router = new Router(app, routes);
app = router.initialize();

app.listen(PORT);
console.log(`Started on port ${PORT}`);

export default app;