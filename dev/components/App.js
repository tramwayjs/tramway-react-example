import {React, Component} from 'tramway-core-react';

/**
 * @class App
 * @extends {Component}
 */
class App extends Component {   
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <p>{this.props.message}</p>
        );
    }
}

App.defaultProps = {
    'message': 'This can be overloaded'
};

export default App;